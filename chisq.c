void GetAmp(string fileName, vector<double>& nxmOFamps, vector<double>& nxmOFdelay, vector<double>& nxmOFchisq, vector<double>& ptOFchisq, vector<double>& ptOFamps, vector<double>& pas1OFamps, vector<double>& pes1OFamps, vector<double>& eventNumVec) {

  //vector<double> nxmOFamps_delay; nxmOFamps_nodelay;
  double EventNumber;
  double PAS1OFamps, PES1OFamps;
  double PSnxmOFdelay, PSnxmOFamps, PSnxmOFchisq;
  double PTOFamps, PTOFchisq;
  double TriggerType, TriggerDetectorNum, TriggerMask;

  double PAS1OFdelay;
  TFile *f1 = new TFile(fileName.c_str(), "READ");
  //TFile *f1 = new TFile("23210107_180316_first10_delayIncluded.root", "READ");
  TTree *t1 = (TTree*)f1->Get("rqDir/zip1");
  TTree *t2 = (TTree*)f1->Get("rqDir/eventTree");

  t1->SetBranchAddress("PAS1OFamps", &PAS1OFamps);
  t1->SetBranchAddress("PES1OFamps", &PES1OFamps);
  t1->SetBranchAddress("PSnxmOFdelay", &PSnxmOFdelay);
  t1->SetBranchAddress("PSnxmOFamps", &PSnxmOFamps);
  t1->SetBranchAddress("PSnxmOFchisq", &PSnxmOFchisq);
  t1->SetBranchAddress("PTOFamps", &PTOFamps);
  t1->SetBranchAddress("PTOFchisq", &PTOFchisq);
  
  t2->SetBranchAddress("EventNumber", &EventNumber);
  t2->SetBranchAddress("TriggerType", &TriggerType);
  t2->SetBranchAddress("TriggerDetectorNum", &TriggerDetectorNum);
  t2->SetBranchAddress("TriggerMask", &TriggerMask);

  cout << t1->GetEntries() << "  " << t2->GetEntries() << endl;
  for(int ctr=0; ctr<t1->GetEntries(); ctr++) {
    t1->GetEntry(ctr);
    t2->GetEntry(ctr);

    if (TriggerType!=1.) continue; // only pass L1 primitives
    if (TriggerDetectorNum!=1) continue; // only pass T5Z2, this is zip1
    if (TriggerMask!=1.) continue; // trigger mask is not in much use currently
    //if (PAS1OFamps<5e-7) continue;
//    if (PSnxmOFdelay/1.6e-6>50 || PSnxmOFdelay/1.6e-6<-50) continue;
    //if (PSnxmOFdelay>0.796e-3) cout << ctr << "   "<< EventNumber << endl;
    nxmOFamps.push_back(PSnxmOFamps);
    nxmOFdelay.push_back(PSnxmOFdelay/1.6e-6);
    nxmOFchisq.push_back(PSnxmOFchisq);
    eventNumVec.push_back(EventNumber);
    ptOFamps.push_back(PTOFamps);
    pas1OFamps.push_back(PAS1OFamps);
    pes1OFamps.push_back(PES1OFamps);
    ptOFchisq.push_back(PTOFchisq);
  }
  cout << "here" << endl;
  f1->Close();
  //cout << nxmOFamps.size() << endl;

}
void plotChisq() {

  TH1D *h1 = new TH1D("h1", "h1", 150, 0, 0.7e-6);
  TH1D *h3 = new TH1D("h3", "h3", 150, 0, 0.7e-6);
  TH1D *h4 = new TH1D("h4", "h4", 150, 0, 0.7e-6);
  TH1D *h5 = new TH1D("h5", "h5", 150, 0, 0.7e-6);
  //TH1D *h1 = new TH1D("h1", "h1", 100, -50, 50);
  TH2D *h2 = new TH2D("h2", "h2", 100, 0, 7e-7, 100, -1e-7, 2e-7);

  vector<double> nxmOFamps_shift, nxmOFamps_noshift;
  vector<double> ptOFamps_shift, ptOFamps_noshift;
  vector<double> pas1OFamps_shift, pas1OFamps_noshift;
  vector<double> pes1OFamps_shift, pes1OFamps_noshift;
  vector<double> nxmOFdelay_shift, nxmOFdelay_noshift;
  vector<double> eventNumVec_shift, eventNumVec_noshift;
  vector<double> nxmOFchisq_shift, nxmOFchisq_noshift;
  vector<double> ptOFchisq_shift, ptOFchisq_noshift;

  GetAmp("Test_23210107_180316_Total_shifted.root", nxmOFamps_shift, nxmOFdelay_shift, nxmOFchisq_shift, ptOFchisq_shift, ptOFamps_shift, pas1OFamps_shift, pes1OFamps_shift, eventNumVec_shift);
  GetAmp("Test_23210107_180316_Total_0shift.root", nxmOFamps_noshift, nxmOFdelay_noshift, nxmOFchisq_noshift, ptOFchisq_noshift, ptOFamps_noshift, pas1OFamps_noshift, pes1OFamps_noshift, eventNumVec_noshift);
    
  //cout << nxmOFamps_shift.size() << "  " << nxmOFamps_noshift.size() << endl;
  for (int i=0; i < nxmOFamps_shift.size(); i++) {
    //if (nxmOFdelay_shift[i]>50 || nxmOFdelay_shift[i]<-50) continue;

    //if (TMath::Abs(nxmOFdelay_noshift[i]) < TMath::Abs(nxmOFdelay_shift[i]) && nxmOFdelay_shift[i]< 0)    cout << i << eventNumVec_shift[i] <<  "  " << eventNumVec_noshift[i] << endl;
    //if (nxmOFdelay_shift[i] < -20) cout << i << eventNumVec_shift[i] <<  "  " << eventNumVec_noshift[i] << endl;
    //if (nxmOFchisq_shift[i]> 1.5e7 && nxmOFchisq_shift[i]<2.5e7) cout <<  nxmOFchisq_shift[i] << "  " << eventNumVec_shift[i] << endl;
    if ( nxmOFchisq_shift[i]>5e6 || nxmOFchisq_noshift[i]>5e6 ) continue; 
    
    if ( ptOFamps_noshift[i]/5.>3.8e-7 || ptOFamps_noshift[i]/5.<3.5e-7) continue;

    //if (eventNumVec_shift[i]>170000 && eventNumVec_shift[i]<180000) 
    //  cout << "["<<eventNumVec_shift[i] <<","<<ptOFamps_noshift[i]/5.<<","<<nxmOFamps_shift[i]<<","<<ptOFamps_noshift[i]/5.-nxmOFamps_shift[i]<<"],";
    h1->Fill(nxmOFamps_shift[i]);
    h3->Fill(nxmOFamps_noshift[i]);
    h4->Fill(ptOFamps_noshift[i]/5.);
    h5->Fill(pas1OFamps_noshift[i]);
    h2->Fill(pas1OFamps_noshift[i]-pes1OFamps_noshift[i], ptOFamps_noshift[i]/5.-nxmOFamps_shift[i]);
    //cout << pes1OFamps_noshift[i] << "  " << pas1OFamps_noshift[i] << "  " << ptOFamps_noshift[i]/5. - nxmOFamps_shift[i] << endl;
    if ( ptOFamps_noshift[i]/5.-nxmOFamps_shift[i]<0.1e-6 && ptOFamps_noshift[i]/5.-nxmOFamps_shift[i]>0.9e-7&& pas1OFamps_noshift[i]-pes1OFamps_noshift[i] > 0.2e-6 ) cout << eventNumVec_shift[i]<<",";
    
    //h2->Fill(nxmOFdelay_shift[i], nxmOFchisq_shift[i]);
  }
  h1->SetTitle("Fitted amp for NxM filter selecting 356 peak (no chisq cuts)");
  h1->GetXaxis()->SetTitle("PSnxmOFamp (#muA)");
  h1->SetStats(0);
  h1->GetYaxis()->SetRangeUser(0, 3000);
  h1->SetLineColor(kRed);
//  h1->Draw();

  h3->SetLineColor(kBlue);
//  h3->Draw("same");

  h4->SetLineColor(kBlack);
//  h4->Draw("same");

  //h5->SetLineColor(kYellow);
  //h5->Draw("same");


//  auto legend = new TLegend(0.7,0.7,0.9,0.9);
//  //legend->SetHeader("The Legend Title","C"); // option "C" allows to center the header
//  legend->AddEntry(h1,"shifted","l");
//  legend->AddEntry(h3,"noshift","l");
//  legend->AddEntry(h4,"PT","l");
//  //legend->AddEntry(h5,"T5Z2-B1","l");
//  legend->Draw();
  

  h2->SetTitle("PT-PSnxm vs. PBS1-PAS1 for 356 peak energy events (with chisq cut)");
  h2->GetXaxis()->SetTitle("amp(PES1-PAS1) #muA");
  h2->GetYaxis()->SetTitle("amp(PT - PSnxm_shift) #muA)");
  h2->SetStats(0);
  h2->Draw("COLZ");
  gPad->SetLogz();
  
  ///TLine *l1 = new TLine(-50,-50,0,0);
  ///TLine *l2 = new TLine(0,0,50,-50);
  ///TLine *l3 = new TLine(0,0,-50,50);
  ///TLine *l4 = new TLine(50,50,0,0);
  ///l1->Draw(); l2->Draw(); l3->Draw(); l4->Draw();

}

void chisq() {
  plotChisq();
  
}
