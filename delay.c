void GetAmp(string fileName, vector<double>& nxmOFamps, vector<double>& nxmOFdelay, vector<double>& eventNumVec) {

  //vector<double> nxmOFamps_delay; nxmOFamps_nodelay;
  double EventNumber;
  double PAS1OFamps;
  double PSnxmOFdelay, PSnxmOFamps, PSnxmOFchisq;
  double TriggerType, TriggerDetectorNum, TriggerMask;

  double PAS1OFdelay;
  TFile *f1 = new TFile(fileName.c_str(), "READ");
  //TFile *f1 = new TFile("23210107_180316_first10_delayIncluded.root", "READ");
  TTree *t1 = (TTree*)f1->Get("rqDir/zip1");
  TTree *t2 = (TTree*)f1->Get("rqDir/eventTree");

  t1->SetBranchAddress("PAS1OFamps", &PAS1OFamps);
  t1->SetBranchAddress("PSnxmOFdelay", &PSnxmOFdelay);
  t1->SetBranchAddress("PSnxmOFamps", &PSnxmOFamps);
  t1->SetBranchAddress("PSnxmOFchisq", &PSnxmOFchisq);
  t2->SetBranchAddress("EventNumber", &EventNumber);
  t2->SetBranchAddress("TriggerType", &TriggerType);
  t2->SetBranchAddress("TriggerDetectorNum", &TriggerDetectorNum);
  t2->SetBranchAddress("TriggerMask", &TriggerMask);

  for(int ctr=0; ctr<t1->GetEntries(); ctr++) {
    t1->GetEntry(ctr);
    t2->GetEntry(ctr);

    if (TriggerType!=1.) continue; // only pass L1 primitives
    if (TriggerDetectorNum!=1) continue; // only pass T5Z2, this is zip1
    if (TriggerMask!=1.) continue; // trigger mask is not in much use currently
    //if (PAS1OFamps<5e-7) continue;
//    if (PSnxmOFdelay/1.6e-6>50 || PSnxmOFdelay/1.6e-6<-50) continue;
    //if (PSnxmOFdelay>0.796e-3) cout << ctr << "   "<< EventNumber << endl;


    nxmOFamps.push_back(PSnxmOFamps);
    nxmOFdelay.push_back(PSnxmOFdelay/1.6e-6);
    eventNumVec.push_back(EventNumber);
  }
  f1->Close();
  //cout << nxmOFamps.size() << endl;

}
void plotDelay() {

  TH1D *h1 = new TH1D("h1", "h1", 25, -10, 15);
  //TH1D *h1 = new TH1D("h1", "h1", 100, -50, 50);
  TH2D *h2 = new TH2D("h2", "h2", 100, -50, 50, 100, -50, 50);

  vector<double> nxmOFamps_shift, nxmOFamps_noshift;
  vector<double> nxmOFdelay_shift, nxmOFdelay_noshift;
  vector<double> eventNumVec_shift, eventNumVec_noshift;

  GetAmp("Test_23210107_180316_Total_shifted.root", nxmOFamps_shift, nxmOFdelay_shift, eventNumVec_shift);
  GetAmp("Test_23210107_180316_Total_0shift.root", nxmOFamps_noshift, nxmOFdelay_noshift, eventNumVec_noshift);
    
  //cout << nxmOFamps_shift.size() << "  " << nxmOFamps_noshift.size() << endl;
  cout << nxmOFamps_shift.size() << "   " << nxmOFamps_noshift.size() << endl;
  for (int i=0; i < nxmOFamps_shift.size(); i++) {
    //if (nxmOFdelay_shift[i]>50 || nxmOFdelay_shift[i]<-50) continue;

    if (TMath::Abs(nxmOFdelay_noshift[i]) < TMath::Abs(nxmOFdelay_shift[i]) && nxmOFdelay_shift[i]< 0)    cout << i << eventNumVec_shift[i] <<  "  " << eventNumVec_noshift[i] << endl;
    //if (nxmOFdelay_shift[i] < -20) cout << i << eventNumVec_shift[i] <<  "  " << eventNumVec_noshift[i] << endl;
    h1->Fill(nxmOFdelay_shift[i]);
    h2->Fill(nxmOFdelay_shift[i], nxmOFdelay_noshift[i]);
  }
  h1->SetTitle("Fitted time delay for  NxM filter (shifted)");
  h1->GetXaxis()->SetTitle("PSnxmOFdelay (bin, or 1.6 #mus)");
  h1->SetStats(0);
  //h1->Draw();

  h2->SetTitle("Fitted time delay for  NxM filter (noshift vs. shifted), chisq<5e6");
  h2->GetXaxis()->SetTitle("PSnxmOFdelay, shifted (bin, or 1.6 #mus)");
  h2->GetYaxis()->SetTitle("PSnxmOFdelay, noshift (bin, or 1.6 #mus)");
  h2->SetStats(0);
  h2->Draw("COLZ");
  
  TLine *l1 = new TLine(-50,-50,0,0);
  TLine *l2 = new TLine(0,0,50,-50);
  TLine *l3 = new TLine(0,0,-50,50);
  TLine *l4 = new TLine(50,50,0,0);
  l1->Draw(); l2->Draw(); l3->Draw(); l4->Draw();

}

void delay() {
  plotDelay();
  
}
