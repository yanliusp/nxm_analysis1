I'm writing some ROOT C++ based analysis script to look at NxM data generated with shifted templates(or more accurately, pulses).

The NxM processed data files were generated locally using CDMSBats/BatCommon. They are:
    1. 23210107_180316_shifted
    2. 23210107_180316_0shift
